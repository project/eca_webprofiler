<?php

namespace Drupal\eca_webprofiler\DataCollector;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\eca_webprofiler\ConfigurableLoggerChannel;
use Drupal\webprofiler\DataCollector\DataCollectorTrait;
use Drupal\webprofiler\DataCollector\HasPanelInterface;
use Drupal\webprofiler\DataCollector\PanelTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

/**
 * Provides EcaDataCollector for the webprofiler of devel.
 */
class EcaDataCollector extends DataCollector implements HasPanelInterface {

  use StringTranslationTrait, DataCollectorTrait, PanelTrait;

  /**
   * The logger channel.
   *
   * @var \Drupal\eca_webprofiler\ConfigurableLoggerChannel
   */
  protected ConfigurableLoggerChannel $logger;

  /**
   * EcaDataCollector constructor.
   *
   * @param \Drupal\eca_webprofiler\ConfigurableLoggerChannel $logger
   *   The logger channel.
   */
  public function __construct(ConfigurableLoggerChannel $logger) {
    $this->logger = $logger;
  }

  /**
   * Gets the number of log entries.
   *
   * @return string
   *   The number of log entries.
   */
  public function getNumberOfLogEntries(): string {
    return (string) count($this->data);
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return 'eca';
  }

  /**
   * {@inheritdoc}
   */
  public function collect(Request $request, Response $response, ?\Throwable $exception = NULL): void {
    foreach ($this->logger->getDataCurrentRequest() as $item) {
      foreach ($item[2] as $key => $value) {
        $item[2][$key] = strip_tags((string) (new FormattableMarkup($value, $item[3])));
      }
      $this->data[] = [
        'message' => $item[1],
        'tokens' => implode('<br>', $item[2]),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPanel(): array {
    if (empty($this->data)) {
      return [
        '#markup' => $this->t('No debugging data available.'),
      ];
    }
    $rows = [];
    foreach ($this->data as $data) {
      $rows[] = [
        [
          'data' => $data['message'],
          'class' => 'webprofiler__key',
        ],
        [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{{ data|raw }}',
            '#context' => [
              'data' => $data['tokens'],
            ],
          ],
          'class' => 'webprofiler__value',
        ],
      ];
    }

    return [
      'Debugging' => [
        '#theme' => 'webprofiler_dashboard_section',
        '#title' => 'Debugging',
        '#data' => [
          '#type' => 'table',
          '#header' => [$this->t('Step'), $this->t('Tokens')],
          '#rows' => $rows,
          '#attributes' => [
            'class' => [
              'webprofiler__table',
            ],
          ],
        ],
      ],
    ];
  }

}
